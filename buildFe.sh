#!/usr/bin/env bash
rm -rf src/main/resources/static/**
yarn --cwd ../news-backend-fe/ build
cp -r ../news-backend-fe/build/* src/main/resources/static/