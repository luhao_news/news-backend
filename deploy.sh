#!/usr/bin/env bash
sh buildFe.sh
mvn clean package -Dmaven.test.skip
cid=$(docker build . -t="news-backend" | grep "Successfully built" | awk '{print $3}')
docker tag $cid swr.cn-north-4.myhuaweicloud.com/luhao_lqr/news-backend:latest
docker push swr.cn-north-4.myhuaweicloud.com/luhao_lqr/news-backend
ssh hw1 "sh /root/deploy/news_backend.sh";