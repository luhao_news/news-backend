package com.ysmull.news.server

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.boot.web.embedded.tomcat.TomcatServletWebServerFactory
import org.springframework.boot.web.server.ErrorPage
import org.springframework.boot.web.servlet.ServletComponentScan
import org.springframework.boot.web.servlet.server.ConfigurableServletWebServerFactory
import org.springframework.context.annotation.Bean
import org.springframework.http.HttpStatus


@SpringBootApplication
@ServletComponentScan
class NewsApplication{
    @Bean
    fun webServerFactory(): ConfigurableServletWebServerFactory {
        val factory = TomcatServletWebServerFactory()
        val error404Page = ErrorPage(HttpStatus.NOT_FOUND, "/")
        factory.addErrorPages(error404Page)
        return factory
    }
}

fun main(args: Array<String>) {
    runApplication<NewsApplication>(*args)
}

