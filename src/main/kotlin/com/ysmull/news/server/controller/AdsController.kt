package com.ysmull.news.server.controller

import com.ysmull.news.server.model.PageList
import com.ysmull.news.server.model.PageQuery
import com.ysmull.news.server.model.WebResponse
import com.ysmull.news.server.model.po.Ads
import com.ysmull.news.server.service.AdsService
import com.ysmull.news.server.util.toWebResponse
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/api/ads")
class AdsController {
    @Autowired
    private lateinit var adsService: AdsService

    @PostMapping("/list")
    @ResponseBody
    internal fun ads(@RequestBody pageQuery: PageQuery): WebResponse<PageList<Ads>> {
        return adsService.getList(pageQuery).toWebResponse()
    }

    @PostMapping("/add")
    @ResponseBody
    internal fun addAds(@RequestBody ads: Ads): WebResponse<Long> {
        return adsService.add(ads).toWebResponse()
    }

    @PostMapping("/delete")
    @ResponseBody
    internal fun deleteAds(id: Int) {
        adsService.delete(id)
    }

    @PostMapping("/update")
    @ResponseBody
    internal fun updateAds(@RequestBody ads: Ads) {
        adsService.update(ads, ads.id!!)
    }

    @PostMapping("/publish")
    @ResponseBody
    internal fun publishAds(id: Long) {
        adsService.publish(id)
    }


}