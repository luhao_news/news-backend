package com.ysmull.news.server.controller

import com.ysmull.news.server.model.PageList
import com.ysmull.news.server.model.PageQuery
import com.ysmull.news.server.model.po.User
import com.ysmull.news.server.model.WebResponse
import com.ysmull.news.server.model.po.Dashboard
import com.ysmull.news.server.service.DashboardService
import com.ysmull.news.server.util.ExcelUtil
import com.ysmull.news.server.util.toWebResponse
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import javax.servlet.http.HttpServletResponse
import javax.servlet.http.HttpSession

@RestController
@RequestMapping("/api/dashboard")
class DashboardController {
    @Autowired
    private lateinit var dashboardService: DashboardService

    @PostMapping("/list")
    @ResponseBody
    internal fun ads(@RequestBody pageQuery: PageQuery, session: HttpSession): WebResponse<PageList<Dashboard>> {
        val user = session.getAttribute("user") as User
        if (user.type == "admin") {
            return dashboardService.getList(pageQuery, null).toWebResponse()
        } else {
            return dashboardService.getList(pageQuery, user.id).toWebResponse()
        }
    }

    @PostMapping("/add")
    @ResponseBody
    internal fun addAds(@RequestBody dashboard: Dashboard): WebResponse<Long> {
        return dashboardService.add(dashboard).toWebResponse()
    }

    @PostMapping("/delete")
    @ResponseBody
    internal fun deleteAds(id: Int) {
        dashboardService.delete(id)
    }

    @PostMapping("/update")
    @ResponseBody
    internal fun updateAds(@RequestBody dashboard: Dashboard) {
        dashboardService.update(dashboard, dashboard.id!!)
    }

    @PostMapping("/export")
    @ResponseBody
    internal fun export(@RequestBody pageQuery: PageQuery, session: HttpSession, response: HttpServletResponse) {
        val user = session.getAttribute("user") as User
        pageQuery.limit = null
        pageQuery.offset = null
        if (user.type == "admin") {
            val result = dashboardService.getList(pageQuery, null).result
            ExcelUtil.export(response, result)
        } else {
            val result = dashboardService.getList(pageQuery, user.id).result
            ExcelUtil.export(response, result)
        }
    }
}
