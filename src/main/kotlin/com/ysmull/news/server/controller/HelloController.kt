package com.ysmull.news.server.controller

import com.ysmull.news.server.util.GitInfoHolder
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.ResponseBody
import org.springframework.web.bind.annotation.RestController


@RestController
@RequestMapping("/util")
class HelloController {

    @Autowired
    private lateinit var gitInfoHolder: GitInfoHolder

    @GetMapping("/version")
    @ResponseBody
    internal fun version(): String {
        return gitInfoHolder.toString()
    }

}
