package com.ysmull.news.server.controller

import com.ysmull.news.server.model.WebResponse
import com.ysmull.news.server.model.WebResponse.Companion.NO_AUTH
import com.ysmull.news.server.model.WebResponse.Companion.SUCCESS
import com.ysmull.news.server.service.LoginService
import com.ysmull.news.server.util.toWebResponse
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import javax.servlet.http.Cookie
import javax.servlet.http.HttpServletResponse
import javax.servlet.http.HttpSession

@RestController
@RequestMapping("/api")
class LoginController {

    @Autowired
    private lateinit var loginService: LoginService

    @PostMapping("/login")
    @ResponseBody
    internal fun login(
        userName: String,
        password: String,
        session: HttpSession,
        response: HttpServletResponse
    ): WebResponse<Any?> {
        val user = loginService.login(userName, password)
        if (user != null) {
            session.setAttribute("user", user)
            val cookie = Cookie("user_type", user.type)
            cookie.path = "/"
            response.addCookie(cookie)
            return ("success" to SUCCESS).toWebResponse()
        } else {
            response.addCookie(Cookie("user_type", null))
            return ("no auth" to NO_AUTH).toWebResponse()
        }
    }

    @GetMapping("/logout")
    @ResponseBody
    internal fun logout(session: HttpSession): WebResponse<Any?> {
        session.removeAttribute("user")
        return ("ok" to SUCCESS).toWebResponse()
    }


}