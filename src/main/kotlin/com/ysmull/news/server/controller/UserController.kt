package com.ysmull.news.server.controller

import com.ysmull.news.server.model.PageList
import com.ysmull.news.server.model.PageQuery
import com.ysmull.news.server.model.po.User
import com.ysmull.news.server.model.WebResponse
import com.ysmull.news.server.service.UserService
import com.ysmull.news.server.util.toWebResponse
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/api/user")
class UserController {
    @Autowired
    private lateinit var userService: UserService

    @PostMapping("/list")
    @ResponseBody
    internal fun ads(@RequestBody pageQuery: PageQuery): WebResponse<PageList<User>> {
        return userService.getList(pageQuery).toWebResponse()
    }

    @PostMapping("/getAllUsers")
    @ResponseBody
    internal fun ads(): WebResponse<List<User>> {
        return userService.getAllUsers().toWebResponse()
    }

    @PostMapping("/add")
    @ResponseBody
    internal fun addAds(@RequestBody user: User): WebResponse<Long> {
        return userService.add(user).toWebResponse()
    }

    @PostMapping("/delete")
    @ResponseBody
    internal fun deleteAds(id: Int) {
        userService.delete(id)
    }

    @PostMapping("/update")
    @ResponseBody
    internal fun updateAds(@RequestBody  user: User) {
        userService.update(user, user.id!!)
    }

}