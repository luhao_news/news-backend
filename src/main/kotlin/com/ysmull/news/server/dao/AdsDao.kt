package com.ysmull.news.server.dao

import com.ysmull.news.server.model.PageList
import com.ysmull.news.server.model.PageQuery
import com.ysmull.news.server.model.po.Ads
import com.ysmull.news.server.model.po.Img
import com.ysmull.news.server.model.po.NewsPO
import com.ysmull.news.server.util.fromJson
import com.ysmull.news.server.util.toUnderScore
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.jdbc.core.BeanPropertyRowMapper
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate
import org.springframework.jdbc.support.GeneratedKeyHolder
import org.springframework.stereotype.Repository


@Repository
class AdsDao {
    @Autowired
    private lateinit var namedParameterJdbcTemplate: NamedParameterJdbcTemplate

    fun getAds(page: PageQuery): PageList<Ads> {
        var sql = "select * from ads"

        val conditions = arrayListOf<String>()

        if (page.type != null) {
            conditions.add("type = '${page.type}'")
        }
        if (page.keyword != null) {
            conditions.add("name like '%${page.keyword}%'")
        }

        val whereSql = if (conditions.size > 0) {
            " where ${conditions.joinToString(separator = " and ")}"
        } else {
            ""
        }

        val orderSql = if (page.sort != null) {
            val split = page.sort.split(":")
            val field = split[0]
            val order = split[1]
            """ order by ${field.toUnderScore()} $order """
        } else {
            """ order by create_time desc """
        }

        val limitSql = " limit ${page.limit} offset ${page.offset} "

        sql += whereSql
        val countSql = """select count(*) as count from ($sql) as t"""
        val count = namedParameterJdbcTemplate.queryForObject(countSql, MapSqlParameterSource(), Int::class.java)

        sql += orderSql
        sql += limitSql
        val result =
            namedParameterJdbcTemplate.query(sql, MapSqlParameterSource(), BeanPropertyRowMapper(Ads::class.java))

        return PageList(count!!, result)
    }

    fun addAds(ads: Ads): Long {
        val sql = "insert into ads(`type`, `name`, `link`, `weight`, `order`, `img_link`) " +
            "      values(:type, :name, :link, :weight, :order, :imgLink)"
        val holder = GeneratedKeyHolder()
        val params = MapSqlParameterSource()
        params.addValue("type", ads.type)
        params.addValue("name", ads.name)
        params.addValue("link", ads.link)
        params.addValue("weight", ads.weight)
        params.addValue("order", ads.order)
        params.addValue("imgLink", ads.imgLink)
        namedParameterJdbcTemplate.update(sql, params, holder)
        return holder.key!!.toLong()
    }

    fun delete(id: Int) {
        val sql = "delete from ads where id = $id"
        namedParameterJdbcTemplate.update(sql, MapSqlParameterSource())
    }

    fun update(ads: Ads, id: Long) {
        val sql = """
            update ads set
            `type`= :type, `name`=:name, `link` = :link, `weight` = :weight, `order` = :order, `img_link` = :imgLink
            where `id` = $id
            """
        val params = MapSqlParameterSource()
        params.addValue("type", ads.type)
        params.addValue("name", ads.name)
        params.addValue("link", ads.link)
        params.addValue("weight", ads.weight)
        params.addValue("order", ads.order)
        params.addValue("imgLink", ads.imgLink)
        namedParameterJdbcTemplate.update(sql, params)
    }

    fun publish(id: Long) {
        val sql = """update ads set `publish` = !`publish` where `id` = $id"""
        namedParameterJdbcTemplate.update(sql, MapSqlParameterSource())
    }
}