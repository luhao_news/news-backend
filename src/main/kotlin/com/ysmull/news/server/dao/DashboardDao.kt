package com.ysmull.news.server.dao

import com.ysmull.news.server.model.PageList
import com.ysmull.news.server.model.PageQuery
import com.ysmull.news.server.model.po.Dashboard
import com.ysmull.news.server.model.po.User
import com.ysmull.news.server.util.toUnderScore
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.dao.EmptyResultDataAccessException
import org.springframework.jdbc.core.BeanPropertyRowMapper
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate
import org.springframework.jdbc.support.GeneratedKeyHolder
import org.springframework.stereotype.Repository


@Repository
class DashboardDao {
    @Autowired
    private lateinit var namedParameterJdbcTemplate: NamedParameterJdbcTemplate

    fun getList(page: PageQuery, userId: Long?): PageList<Dashboard> {
        var sql = "select * from dashboard"

        val conditions = arrayListOf<String>()

        if (userId != null) {
            conditions.add("user_id = $userId")
        }

        if (page.userId != null) {
            conditions.add("user_id = ${page.userId}")
        }
        if (page.startTime != null) {
            conditions.add("create_time between '${page.startTime}' and '${page.endTime}'")
        }

        if (page.keyword != null) {
            conditions.add("channel_code like '%${page.keyword}%'")
        }

        val whereSql = if (conditions.size > 0) {
            " where ${conditions.joinToString(separator = " and ")}"
        } else {
            ""
        }

        val orderSql = if (page.sort != null) {
            val split = page.sort.split(":")
            val field = split[0]
            val order = split[1]
            """ order by ${field.toUnderScore()} $order """
        } else {
            """ order by create_time desc """
        }

        var limitSql = " "
        if (page.limit != null) {
            limitSql = " limit ${page.limit} offset ${page.offset} "
        }

        sql += whereSql
        val countSql = """select count(*) as count from ($sql) as t"""
        val count = namedParameterJdbcTemplate.queryForObject(countSql, MapSqlParameterSource(), Int::class.java)

        sql += orderSql
        sql += limitSql
        val result =
            namedParameterJdbcTemplate.query(sql, MapSqlParameterSource(), BeanPropertyRowMapper(Dashboard::class.java))

        return PageList(count!!, result)
    }


    fun add(dashboard: Dashboard): Long {
        val sql =
            "insert into dashboard(`channel_name`, `channel_code`, `uv`, `earning`, `user_id`, `create_time`) values(:channelName, :channelCode, :uv, :earning, :userId, :createTime)"
        val holder = GeneratedKeyHolder()
        val params = MapSqlParameterSource()
        params.addValue("channelName", dashboard.channelName)
        params.addValue("channelCode", dashboard.channelCode)
        params.addValue("uv", dashboard.uv)
        params.addValue("earning", dashboard.earning)
        params.addValue("userId", dashboard.userId)
        params.addValue("createTime", dashboard.createTime)
        namedParameterJdbcTemplate.update(sql, params, holder)
        return holder.key!!.toLong()
    }

    fun delete(id: Int) {
        val sql = "delete from dashboard where id = $id"
        namedParameterJdbcTemplate.update(sql, MapSqlParameterSource())
    }

    fun update(dashboard: Dashboard, id: Long) {
        val sql = """
            update dashboard set
            `channel_name`=:channelName,
             `channel_code` = :channelCode,
             `uv` = :uv,
             `earning` = :earning,
             `user_id` = :userId,
             `create_time` = :createTime
            where `id` = $id
            """
        val params = MapSqlParameterSource()
        params.addValue("channelName", dashboard.channelName)
        params.addValue("channelCode", dashboard.channelCode)
        params.addValue("uv", dashboard.uv)
        params.addValue("earning", dashboard.earning)
        params.addValue("userId", dashboard.userId)
        params.addValue("createTime", dashboard.createTime)
        namedParameterJdbcTemplate.update(sql, params)
    }

}
