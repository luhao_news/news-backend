package com.ysmull.news.server.dao

import com.ysmull.news.server.model.PageList
import com.ysmull.news.server.model.PageQuery
import com.ysmull.news.server.model.po.User
import com.ysmull.news.server.util.toUnderScore
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.dao.EmptyResultDataAccessException
import org.springframework.jdbc.core.BeanPropertyRowMapper
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate
import org.springframework.jdbc.support.GeneratedKeyHolder
import org.springframework.stereotype.Repository


@Repository
class UserDao {
    @Autowired
    private lateinit var namedParameterJdbcTemplate: NamedParameterJdbcTemplate

    fun getUserByUserName(userName: String): User? {
        return try {
            val sql = "select * from backend_user where user_name = :userName";
            namedParameterJdbcTemplate.queryForObject(
                sql,
                MapSqlParameterSource("userName", userName),
                BeanPropertyRowMapper(User::class.java)
            )
        } catch (e: EmptyResultDataAccessException) {
            null
        }
    }

    fun getAllUsers(): List<User> {
        val sql = "select * from backend_user where type != 'admin'"
        return namedParameterJdbcTemplate.query(sql, MapSqlParameterSource(), BeanPropertyRowMapper(User::class.java))
    }

    fun getList(page: PageQuery): PageList<User> {
        var sql = "select * from backend_user"

        val conditions = arrayListOf<String>()

        conditions.add("type = 'normal'")
        if (page.keyword != null) {
            conditions.add("user_name like '%${page.keyword}%'")
        }

        val whereSql = if (conditions.size > 0) {
            " where ${conditions.joinToString(separator = " and ")}"
        } else {
            ""
        }

        val orderSql = if (page.sort != null) {
            val split = page.sort.split(":")
            val field = split[0]
            val order = split[1]
            """ order by ${field.toUnderScore()} $order """
        } else {
            """ order by create_time desc """
        }

        val limitSql = " limit ${page.limit} offset ${page.offset} "

        sql += whereSql
        val countSql = """select count(*) as count from ($sql) as t"""
        val count = namedParameterJdbcTemplate.queryForObject(countSql, MapSqlParameterSource(), Int::class.java)

        sql += orderSql
        sql += limitSql
        val result =
            namedParameterJdbcTemplate.query(sql, MapSqlParameterSource(), BeanPropertyRowMapper(User::class.java))

        return PageList(count!!, result)
    }


    fun addUser(user: User): Long {
        val sql = "insert into backend_user(`user_name`, `password`, `type`) values(:userName, :password, :type)"
        val holder = GeneratedKeyHolder()
        val params = MapSqlParameterSource()
        params.addValue("userName", user.userName)
        params.addValue("password", user.password)
        params.addValue("type", user.type)
        namedParameterJdbcTemplate.update(sql, params, holder)
        return holder.key!!.toLong()
    }

    fun delete(id: Int) {
        val sql = "delete from backend_user where id = $id"
        namedParameterJdbcTemplate.update(sql, MapSqlParameterSource())
    }

    fun update(user: User, id: Long) {
        val sql = """
            update backend_user set
            `user_name`=:userName, `password` = :password
            where `id` = $id
            """
        val params = MapSqlParameterSource()
        params.addValue("userName", user.userName)
        params.addValue("password", user.password)
        namedParameterJdbcTemplate.update(sql, params)
    }

}