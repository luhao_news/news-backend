package com.ysmull.news.server.filter

import com.alibaba.fastjson.JSON
import com.ysmull.news.server.model.WebResponse
import com.ysmull.news.server.model.WebResponse.Companion.SERVER_ERROR
import com.ysmull.news.server.util.toJson
import org.slf4j.LoggerFactory
import org.springframework.core.annotation.Order
import org.springframework.http.HttpStatus
import javax.servlet.*
import javax.servlet.annotation.WebFilter
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse


@Order(1)
@WebFilter(filterName = "validation", urlPatterns = ["/*"], asyncSupported = true)
class ValidateFilter : Filter {
    private val logger = LoggerFactory.getLogger(ValidateFilter::class.java)
    override fun init(filterConfig: FilterConfig?) {
        // do nothing
    }

    override fun doFilter(servletRequest: ServletRequest, servletResponse: ServletResponse, filterChain: FilterChain) {
        servletResponse as HttpServletResponse
        try {
            servletRequest as HttpServletRequest

//            if (true) {
            if (!servletRequest.requestURI.startsWith("/api") || ignore_urls.contains(servletRequest.requestURI)) {
                filterChain.doFilter(servletRequest, servletResponse)
            } else {
                val user = servletRequest.session.getAttribute("user")
                if (user == null) {
                    servletResponse.status = HttpStatus.UNAUTHORIZED.value()
                    servletResponse.writer.write(object {
                        val redirect = "/login"
                    }.toJson())
                } else {
                    filterChain.doFilter(servletRequest, servletResponse)
                }
            }
        } catch (e: Exception) {
            logger.error(e.message)
            servletResponse.status = HttpStatus.INTERNAL_SERVER_ERROR.value()
            servletResponse.writer.write(JSON.toJSONString(WebResponse(null, e.message, SERVER_ERROR)))
        }
    }

    override fun destroy() {
        // do nothing
    }

    companion object {
        val ignore_urls = arrayOf("/api/logout", "/api/login", "/api/version")
    }
}
