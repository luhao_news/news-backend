package com.ysmull.news.server.model


data class PageList<T>(
    var count: Int,
    var result: List<T>
)