package com.ysmull.news.server.model

class PageQuery(
    val type: String?,
    var offset: Int? = 0,
    var limit: Int? = 10,
    val keyword: String?,
    val sort: String?,
    val filter: String?,
    val startTime: String?,
    val endTime: String?,
    val userId: Long?
)
