package com.ysmull.news.server.model

import java.io.Serializable

class WebResponse<T>(var data: T?, var msg: String? = null, var code :Int = SUCCESS) : Serializable {



    override fun toString(): String {
        return "WebResponse{" +
            "code=" + code +
            ", msg='" + msg + '\''.toString() +
            ", data=" + data +
            '}'.toString()
    }


    companion object {
        const val CLIENT_ERROR = 400
        const val NOT_FOUND = 404
        const val NO_AUTH = 401
        const val SERVER_ERROR = 500
        const val SUCCESS = 200
    }
}

