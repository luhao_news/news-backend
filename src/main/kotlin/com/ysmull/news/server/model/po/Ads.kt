package com.ysmull.news.server.model.po

import com.ysmull.news.server.annotation.NoArg
import java.util.*

@NoArg
data class Ads(
    var id: Long? = null,
    var type: String,
    var name: String,
    var link: String,
    var publish: Boolean,
    var weight: Double?,
    var order: Int?,
    var imgLink: String?,
    var createTime: Date?,
    var modifyTime: Date?,
    var times: Int?
)
