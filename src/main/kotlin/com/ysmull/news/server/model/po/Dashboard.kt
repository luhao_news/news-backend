package com.ysmull.news.server.model.po

import com.ysmull.news.server.annotation.NoArg
import java.util.*

@NoArg
data class Dashboard(
    var id: Long? = null,
    var channelName: String,
    var channelCode: String,
    var userId: Long,
    var earning: Double,
    var uv: Int,
    var createTime: Date?
)
