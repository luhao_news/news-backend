package com.ysmull.news.server.model.po

import com.ysmull.news.server.annotation.NoArg


data class Img(
    val height: Int,
    val width: Int,
    val name: String,
    val src: String
)

@NoArg
data class NewsPO(
    val id: Long? = null,
    val type: String,
    val title: String,
    val description: String,
    val content: String,
    val pics: List<Img>?
)