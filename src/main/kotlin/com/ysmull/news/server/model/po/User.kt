package com.ysmull.news.server.model.po

import com.ysmull.news.server.annotation.NoArg
import java.io.Serializable
import java.util.*

@NoArg
data class User (
    var id: Long? = null,
    var userName: String,
    var password: String,
    var type: String = "normal",
    var createTime: Date?
): Serializable