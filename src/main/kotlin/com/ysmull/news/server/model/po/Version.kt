package com.ysmull.news.server.model.po

import com.ysmull.news.server.annotation.NoArg

@NoArg
data class Version(
    var version: String,
    var url: String
)