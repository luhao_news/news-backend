package com.ysmull.news.server.service

import com.ysmull.news.server.dao.AdsDao
import com.ysmull.news.server.model.PageList
import com.ysmull.news.server.model.PageQuery
import com.ysmull.news.server.model.po.Ads
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.util.concurrent.ThreadLocalRandom

@Service
class AdsService {

    @Autowired
    private lateinit var adsDao: AdsDao


    internal fun getList(pageQuery: PageQuery): PageList<Ads> {
        return adsDao.getAds(pageQuery)
    }

    internal fun add(ads: Ads): Long {
        return adsDao.addAds(ads)
    }

    internal fun delete(id: Int) {
        adsDao.delete(id)
    }

    internal fun update(ads: Ads, id: Long) {
        adsDao.update(ads, id)
    }

    internal fun publish(id: Long) {
        adsDao.publish(id)
    }

}