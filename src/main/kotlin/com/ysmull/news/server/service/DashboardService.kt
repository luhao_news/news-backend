package com.ysmull.news.server.service

import com.ysmull.news.server.dao.DashboardDao
import com.ysmull.news.server.model.PageList
import com.ysmull.news.server.model.PageQuery
import com.ysmull.news.server.model.po.Dashboard
import com.ysmull.news.server.model.po.User
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class DashboardService {

    @Autowired
    private lateinit var dashboardDao: DashboardDao


    internal fun getList(pageQuery: PageQuery, userId: Long?): PageList<Dashboard> {
        return dashboardDao.getList(pageQuery, userId)
    }

    internal fun add(dashboard: Dashboard): Long {
        return dashboardDao.add(dashboard)
    }

    internal fun delete(id: Int) {
        dashboardDao.delete(id)
    }

    internal fun update(dashboard: Dashboard, id: Long) {
        dashboardDao.update(dashboard, id)
    }

}