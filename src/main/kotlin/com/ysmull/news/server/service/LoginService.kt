package com.ysmull.news.server.service

import com.ysmull.news.server.dao.UserDao
import com.ysmull.news.server.model.po.User
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Repository


@Repository
class LoginService {
    @Autowired
    private lateinit var userDao: UserDao

    fun login(userName: String, password: String): User? {
        val user = userDao.getUserByUserName(userName)
        return if (user != null && user.password == password) {
            user
        } else {
            null
        }
    }
}