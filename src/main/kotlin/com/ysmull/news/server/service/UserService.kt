package com.ysmull.news.server.service

import com.ysmull.news.server.dao.UserDao
import com.ysmull.news.server.model.PageList
import com.ysmull.news.server.model.PageQuery
import com.ysmull.news.server.model.po.User
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class UserService {

    @Autowired
    private lateinit var userDao: UserDao


    internal fun getList(pageQuery: PageQuery): PageList<User> {
        return userDao.getList(pageQuery)
    }

    internal fun getAllUsers(): List<User> {
        return userDao.getAllUsers()
    }

    internal fun add(user: User): Long {
        return userDao.addUser(user)
    }

    internal fun delete(id: Int) {
        userDao.delete(id)
    }

    internal fun update(user: User, id: Long) {
        userDao.update(user, id)
    }

}