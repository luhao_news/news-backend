package com.ysmull.news.server.util

import com.fasterxml.jackson.databind.ObjectMapper
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.ysmull.news.server.model.WebResponse
import org.apache.poi.ss.usermodel.Cell
import java.math.BigInteger
import java.security.MessageDigest
import java.util.*


var gson = Gson()

var objectMapper = ObjectMapper()

fun <T> T.toJson(): String = objectMapper.writeValueAsString(this)

inline fun <reified T> String?.fromJson(): T? =
    if (this == null) null else gson.fromJson(this, object : TypeToken<T>() {}.type)


fun Any.print(postfix: String = "") = println(this.toString() + postfix)

fun Pair<String, Int>.toWebResponse(): WebResponse<Any?> = WebResponse(data=null, code=this.second, msg=this.first)


fun <T> T?.toWebResponse(): WebResponse<T> = WebResponse(this)



fun String.md5(): String {
    val md = MessageDigest.getInstance("MD5")
    return BigInteger(1, md.digest(toByteArray())).toString(16).padStart(32, '0')
}

fun String.toUnderScore(): String {
    return this.replace("([^_A-Z])([A-Z])".toRegex(), "$1_$2").toLowerCase()
}


fun Cell.setCellValue(value: Any) {
    when (value) {
        is Int, is Double, is Float, is Long -> this.setCellValue(value as Double)
        is String -> this.setCellValue(value)
        is Date -> this.setCellValue(value)
        is Boolean -> this.setCellValue(value)
        else -> this.setCellValue("")
    }
}
