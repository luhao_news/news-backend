package com.ysmull.news.server.util

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource

fun <T> createParameterSource(receiver: T, block: MutableMap<String, Any?>.(T) -> Unit): MapSqlParameterSource {
    val source = MapSqlParameterSource()
    val map = mutableMapOf<String, Any?>()
    map.block(receiver)
    return source.addValues(map)
}

fun <T> createBatchParameterSource(
    receiverList: List<T>,
    block: MutableMap<String, Any?>.(T) -> Unit
): Array<MapSqlParameterSource> {
    return receiverList.map { item ->
        createParameterSource(item, block)
    }.toTypedArray()
}