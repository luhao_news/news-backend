package com.ysmull.news.server.util


import com.ysmull.news.server.model.po.Dashboard
import org.apache.poi.hssf.usermodel.HSSFWorkbook
import java.time.LocalDateTime
import java.time.ZoneId
import java.time.format.DateTimeFormatter
import java.util.*
import javax.servlet.http.HttpServletResponse


class ExcelUtil {
    companion object {

        private val formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")
        private fun convertDate(dateToConvert: Date?): LocalDateTime? {
            return dateToConvert?.toInstant()
                ?.atZone(ZoneId.systemDefault())
                ?.toLocalDateTime()
        }

        fun export(response: HttpServletResponse, data: List<Dashboard>) {

            DateTimeFormatter.ofPattern("MM-dd HH:mm:ss")
            val workbook = HSSFWorkbook() //创建一个工作薄
            val sheet = workbook.createSheet("sheet1") //创建一个sheet页
            val header = sheet.createRow(0)
            header.createCell(0).setCellValue("日期")
            header.createCell(1).setCellValue("用户")
            header.createCell(2).setCellValue("渠道名称")
            header.createCell(3).setCellValue("渠道号")
            header.createCell(4).setCellValue("点击量")
            header.createCell(5).setCellValue("结算收入")
            var i = 1
            data.forEach { rowData ->
                val row = sheet.createRow(i)
                row.createCell(0).setCellValue(convertDate(rowData.createTime)?.format(formatter))
                row.createCell(1).setCellValue(rowData.userId.toDouble())
                row.createCell(2).setCellValue(rowData.channelName)
                row.createCell(3).setCellValue(rowData.channelCode)
                row.createCell(4).setCellValue(rowData.uv.toDouble())
                row.createCell(5).setCellValue(rowData.earning)
                i += 1
            }
            response.addHeader("content-type", "application/vnd.ms-excel")
            response.addHeader("Cache-Control", "no-cache, no-store, must-revalidate");
            response.addHeader("Content-Disposition", String.format("attachment; filename=\"%s\"", "export.xls"));
            response.addHeader("Pragma", "no-cache");
            response.addHeader("Expires", "0");
            workbook.write(response.outputStream)
        }
    }
}
